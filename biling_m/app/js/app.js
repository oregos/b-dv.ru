import Swiper from 'swiper';

import Micromodal from 'micromodal';

import jump from 'jump.js';

import { Fancybox } from "@fancyapps/ui";

document.addEventListener('DOMContentLoaded', () => {


	/*--------------------------Шапка при скролле----------------------------*/
	window.addEventListener('scroll', function(e) {
		if (window.scrollY > 100) {
			document.querySelector('.header').classList.add('scroll');
		}
		else{
			document.querySelector('.header').classList.remove('scroll');
		}
	});


	/*--------------------------Открыть и закрыть меню----------------------------*/
	document.querySelector('.menu_show_js').addEventListener('click', function (e) {
		e.preventDefault();
		document.querySelector('.mob_menu').style.display='block';
		document.querySelector('.mob_menu').classList.remove('fadeOut');
		document.querySelector('.mob_menu').classList.add('fadeIn');
		document.querySelector('html').classList.add('no_scroll');
	});

	document.querySelector('.menu_hide_js').addEventListener('click', function (e) {
		e.preventDefault();
		document.querySelector('.mob_menu').classList.remove('fadeIn');
		document.querySelector('.mob_menu').classList.add('fadeOut');
		document.querySelector('html').classList.remove('no_scroll');
		setTimeout(() => {
			document.querySelector('.mob_menu').style.display="none";
		},250)
	})


	/*---------------------Слайдер на главной-------------------------*/
	const home_slider = new Swiper(".home_slider", {
		slidesPerView: 1,
		effect: "fade",
		spaceBetween: 0,
		pagination: {
			el: ".home_slider_dots",
		},
		autoplay: {
			delay: 5000,
			disableOnInteraction: false,
		},
	});


	/*---------------------Слайдер выбор жк------------------------*/
	new Swiper(".home_choice_slider", {
		slidesPerView: 1,
		spaceBetween: 0,
	});


	/*----------------------------------Плавная прокрутка--------------------------------------*/
	document.querySelectorAll('.scroll_js').forEach(item => {
		item.addEventListener('click', function (e) {
			jump(item.getAttribute('href'), {
				offset: -80
			})
		})
	});


	/*---------------------Одинаковая высота заголовков в документе-------------------------*/
	const heightBlock = [];
	document.querySelectorAll('.height_js').forEach(item => {
		heightBlock.push(item.clientHeight)
	});
	const largest = Math.max.apply(Math, heightBlock)
	document.querySelectorAll('.height_js').forEach(item => {
		item.style.height = largest + 'px';
	});


	/*---------------------Выпадающий список-------------------------*/
	document.querySelectorAll('.select_js').forEach(item => {
		const select_list = item.querySelector('.select_list');
		const select_links = select_list.querySelectorAll('li');
		const input = item.querySelector('input');
		input.addEventListener('click', function (e) {
			e.preventDefault();
			select_list.classList.add('fadeIn');
			select_list.classList.add('show');
		})
		select_links.forEach(select_link => {
			select_link.addEventListener('click', function (e) {
				input.value = select_link.innerHTML;
				select_list.classList.remove('fadeIn');
				select_list.classList.remove('show');
			})
		})
	});


	/*---------------------------Анимация в контактах------------------------------------*/
	if(document.querySelector('.map_top_js')){
		document.querySelector('.map_top_js').addEventListener('click', function (e) {
			e.preventDefault();
			document.querySelector('.contact_wrap.bottom').classList.add('hide');
			document.querySelector('.contact_wrap.top').classList.remove('hide');
		});
	}
	if(document.querySelector('.map_bottom_js')){
		document.querySelector('.map_bottom_js').addEventListener('click', function (e) {
			e.preventDefault();
			document.querySelector('.contact_wrap.bottom').classList.remove('hide');
			document.querySelector('.contact_wrap.top').classList.add('hide');
		})
	}


	/*---------------------------Молальные окна------------------------------*/
	MicroModal.init({
		closeTrigger: 'data-micromodal-close',
		disableScroll: true,
		disableFocus: true,
		awaitOpenAnimation: true,
		awaitCloseAnimation: true,
	});


	/*-----------------------Ход строительства слайдер-----------------------------------*/
const progress_slider = new Swiper(".progress_slider");
progress_slider.on('slideChange', function () {
	document.querySelectorAll('.month_js').forEach(item => {
		item.classList.remove('active');
	})
	let index = progress_slider.activeIndex + 1;
	document.querySelector('.month_js:nth-child('+ index +')').classList.add('active');
});


/*-------------------Переключение месяцев--------------------------------*/
document.querySelectorAll('.month_js').forEach(item => {
	item.addEventListener('click', function () {
		if(!item.classList.contains('active')){

			document.querySelectorAll('.month_js').forEach(item => {
				item.classList.remove('active');
			})

			item.classList.add('active');


			let index;
			for (var i = 0; i < document.querySelectorAll('.month_js').length; i++) {
				if(document.querySelectorAll('.month_js')[i].classList.contains('active')){
					index = i;
				}
			}

			progress_slider.slideTo(index);

		}
		

	})
})

/*----------------------Слайдер прогресса назад----------------------------*/
document.querySelector('.prev_js').addEventListener('click', function () {
	progress_slider.slidePrev();
})
/*----------------------Слайдер прогресса назад----------------------------*/
document.querySelector('.next_js').addEventListener('click', function () {
	progress_slider.slideNext();
})

})
