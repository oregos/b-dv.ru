import Swiper from 'swiper';

import Micromodal from 'micromodal';

import jump from 'jump.js';

import { Fancybox } from "@fancyapps/ui";


document.addEventListener('DOMContentLoaded', () => {



	/*--------------------Прелоадер-------------------------*/
	if(document.querySelector('.preloader')){
		document.querySelector('html').classList.add('no_scroll');
		setTimeout(() => {
			document.querySelector('.preloader_block').classList.add('show');
		},500)
		setTimeout(() => {
			document.querySelector('.preloader_block').classList.add('step_2');
		},1500)
		setTimeout(() => {
			document.querySelector('.preloader').style.opacity = "0";


			/*---------------------Слайдер на главной-------------------------*/
			const home_slider = new Swiper(".home_slider", {
				slidesPerView: 1,
				effect: "fade",
				spaceBetween: 0,
				pagination: {
					el: ".home_slider_dots",
				},
				autoplay: {
					delay: 5000,
					disableOnInteraction: false,
				},
			});
			document.querySelector('html').classList.remove('no_scroll');
		},6500)
		setTimeout(() => {
			document.querySelector('.preloader').remove();
		},8500)
	}


	/*----------------------------------Плавная прокрутка--------------------------------------*/
	document.querySelectorAll('.scroll_js').forEach(item => {
		item.addEventListener('click', function (e) {
			jump(item.getAttribute('href'))
		})
	})


	/*---------------------Слайдер-------------------------*/
	const project_slider = new Swiper(".project_slider", {
		slidesPerView: 1,
		effect: "fade",
		spaceBetween: 0,
		pagination: {
			el: ".home_slider_dots",
		},
		autoplay: {
			delay: 5000,
			disableOnInteraction: false,
		},
	});


	/*---------------------Одинаковая высота заголовков в документе-------------------------*/
	const heightBlock = [];
	document.querySelectorAll('.height_js').forEach(item => {
		heightBlock.push(item.clientHeight)
	});
	const largest = Math.max.apply(Math, heightBlock)
	document.querySelectorAll('.height_js').forEach(item => {
		item.style.height = largest + 'px';
	});


	/*---------------------Выпадающий список-------------------------*/
	document.querySelectorAll('.select_js').forEach(item => {
		const select_list = item.querySelector('.select_list');
		const select_links = select_list.querySelectorAll('li');
		const input = item.querySelector('input');
		input.addEventListener('click', function (e) {
			e.preventDefault();
			select_list.classList.add('fadeIn');
			select_list.classList.add('show');
		})
		select_links.forEach(select_link => {
			select_link.addEventListener('click', function (e) {
				input.value = select_link.innerHTML;
				select_list.classList.remove('fadeIn');
				select_list.classList.remove('show');
			})
		})
	});


	/*-----------------------Анмация при показе страницы---------------------------*/
	function page_show(block, steps = 1){
		let element = document.querySelector(block);
		if(element){
			if(!element.classList.contains('show')){
				if(steps == 1){
					setTimeout(() => {
						element.classList.add('show');
					}, 500);
				}
				if(steps == 2){
					setTimeout(() => {
						element.classList.add('show');
					}, 500);
					setTimeout(() => {
						element.classList.add('step_2');
					}, 1500);
				}
				if(steps == 3){
					setTimeout(() => {
						element.classList.add('show');
					}, 500);
					setTimeout(() => {
						element.classList.add('step_2');
					}, 1500);
					setTimeout(() => {
						element.classList.add('step_3');
					}, 2500);
				}
			}
		}
	}

	page_show('#contact', 1);


	/*--------------------------Переключение в квартирах----------------------------*/
	function change_fill(block, color){
		document.querySelector(block).setAttribute('fill', color)
	}

	if(document.querySelector('#house_left')){
		document.querySelector('#house_left').addEventListener('click', function (e) {
			e.preventDefault();
			change_fill('#house_right_rect', '#dbdbdb');
			change_fill('#house_right_num', '#000');
			change_fill('#house_right_text', '#848484');
			change_fill('#house_left_rect', '#B91A1A');
			change_fill('#house_left_num', '#fff');
			change_fill('#house_left_text', '#B91A1A');
			document.querySelector('.min_house').classList.remove('right');
			document.querySelector('.min_house').classList.add('left');
			document.querySelector('.sect_js').innerHTML = 1;
			document.querySelector('.apart_sheme_row').classList.remove('right');
			document.querySelector('.apart_sheme_row').classList.add('left');

		});
	}
	if(document.querySelector('#house_right')){
		document.querySelector('#house_right').addEventListener('click', function (e) {
			e.preventDefault();
			change_fill('#house_left_rect', '#dbdbdb');
			change_fill('#house_left_num', '#000');
			change_fill('#house_left_text', '#848484');
			change_fill('#house_right_rect', '#B91A1A');
			change_fill('#house_right_num', '#fff');
			change_fill('#house_right_text', '#B91A1A');
			document.querySelector('.min_house').classList.remove('left')
			document.querySelector('.min_house').classList.add('right')
			document.querySelector('.sect_js').innerHTML = 2;
			document.querySelector('.apart_sheme_row').classList.remove('left')
			document.querySelector('.apart_sheme_row').classList.add('right')
		});
	}
	


	/*---------------------Всплывашка на этажах-------------------------*/
// 	document.querySelectorAll('.hover_modal_js').forEach(item => {
// 	//Появление ховера при наведении
// 	item.addEventListener('mouseover', function (e) {
// 		document.querySelectorAll('.hover_modal_js').forEach(item => {
// 			//Скрываем ховер из остальных этажей
// 			item.classList.remove('hover')
// 		});
// 		//Добавить ховер на текущем
// 		this.classList.add('hover');
// 	});

// });

//Показать попап при клике на кнопку
document.querySelectorAll('.view_modal_js').forEach(item => {
	item.addEventListener('click', function (e) {
		e.preventDefault();
			//Убрать прошлые окна
			document.querySelectorAll('.choices_popup').forEach(item => {
				item.style.display = "none";
			})
			document.querySelector('.choices_popup[data-index="' + this.getAttribute('data-index') + '"]').classList.add('fadeIn')
			document.querySelector('.choices_popup[data-index="' + this.getAttribute('data-index') + '"]').style.display = "block";

			
		});
});

//Убрать попап когда курсор вышел за пределы окна
document.querySelectorAll('.choices_popup').forEach(item => {
	item.addEventListener('mouseleave', function (e) {
		item.style.display = "none";
	})
})


/*---------------------------Молальные окна------------------------------*/
MicroModal.init({
	closeTrigger: 'data-micromodal-close',
	disableScroll: true,
	disableFocus: true,
	awaitOpenAnimation: true,
	awaitCloseAnimation: true,
});


/*-----------------------Ход строительства слайдер-----------------------------------*/
const progress_slider = new Swiper(".progress_slider");
progress_slider.on('slideChange', function () {
	document.querySelectorAll('.month_js').forEach(item => {
		item.classList.remove('active');
	})
	let index = progress_slider.activeIndex + 1;
	document.querySelector('.month_js:nth-child('+ index +')').classList.add('active');
});


/*-------------------Переключение месяцев--------------------------------*/
document.querySelectorAll('.month_js').forEach(item => {
	item.addEventListener('click', function () {
		if(!item.classList.contains('active')){

			document.querySelectorAll('.month_js').forEach(item => {
				item.classList.remove('active');
			})

			item.classList.add('active');


			let index;
			for (var i = 0; i < document.querySelectorAll('.month_js').length; i++) {
				if(document.querySelectorAll('.month_js')[i].classList.contains('active')){
					index = i;
				}
			}

			progress_slider.slideTo(index);

		}
		//progress_slider.slideNext();
		//progress_slider.slidePrev();
		

	})
})

/*----------------------Слайдер прогресса назад----------------------------*/
document.querySelector('.prev_js').addEventListener('click', function () {
	progress_slider.slidePrev();
})
/*----------------------Слайдер прогресса назад----------------------------*/
document.querySelector('.next_js').addEventListener('click', function () {
	progress_slider.slideNext();
})






})
